import React from "react";
import { Link } from "react-router-dom";

const ContentHeader = ({
  titulo,
  breadCrumb1,
  breadCrumb2,
  breadCrumb3,
  breadCrumb4,
  breadCrumb5 = "Funciones",
  breadCrumb6 = "Asincronia",
  /* Las "Funciones y Asincronia" se declaran en este componente, para que en el caso
  de que la variable breadCrumb5 y breadCrumb6 no esten en todos los componentes esta
  sea declarada por defecto en cada vista, de manera similar sucede con la ruta 5 y 6 */
  ruta1,
  ruta3,
  ruta4,
  ruta5 = "/funciones-admin",
  ruta6 = "/asincronia-admin",
}) => {
  return (
    <section className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-4">
            <h1>{titulo}</h1>
          </div>
          <div className="col-sm-8">
            <ol className="breadcrumb float-sm-right">
              <li className="breadcrumb-item">
                <Link to={ruta1}>{breadCrumb1}</Link>
              </li>
              <li className="breadcrumb-item active">{breadCrumb2}</li>
              <li className="breadcrumb-item">
                <Link to={ruta3}>{breadCrumb3}</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to={ruta4}>{breadCrumb4}</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to={ruta5}>{breadCrumb5}</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to={ruta6}>{breadCrumb6}</Link>
              </li>
            </ol>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ContentHeader;
