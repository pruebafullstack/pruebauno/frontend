import React from "react";

const Footer = () => {
  return (
    <footer className="main-footer">
      <div className="float-right d-none d-sm-block">
        <b>Version</b> 3.2.0
      </div>
      <strong>
        Copyright © 2022 <a href="https://adminlte.io">TUDOCTORONLINE</a>.
      </strong>{" "}
      &nbsp;         
      <a href="mailto:foo@bar.com">Escribeme Email</a>
      &nbsp;         
      All rights reserved.
    </footer>
  );
};

export default Footer;
