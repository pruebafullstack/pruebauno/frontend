//import logo from './logo.svg';
//import './App.css';
import React, { Fragment } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Login from "./paginas/auth/Login";
import CrearCuenta from "./paginas/auth/CrearCuenta";
import Home from "./paginas/Home";
import CitasAdmin from "../src/paginas/citas/CitasAdmin"
import CitasCrear from "../src/paginas/citas/CitasCrear"
import CitasEditar from "../src/paginas/citas/CitasEditar"
import TratamientosAdmin from "./paginas/tratamiento/TratamientosAdmin";
import TratamientosCrear from "./paginas/tratamiento/TratamientosCrear";
import TratamientosEditar from "./paginas/tratamiento/TratamientosEditar";
import PersonasEditar from "./paginas/personas/PersonasEditar";
import PersonasAdmin from "./paginas/personas/PersonasAdmin";
import PersonasCrear from "./paginas/personas/PersonasCrear";

import FuncionesAdmin from "./paginas/ejerciciosFunciones/FuncionesAdmin";
import AsincroniaAdmin from "./paginas/ejerciciosAsincronia/AsincroniaAdmin";


function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path="/" exact element={<Login></Login>}></Route>
          <Route path="/crear-cuenta" exact element={<CrearCuenta></CrearCuenta>}></Route>
          <Route path="/home" exact element={<Home></Home>}></Route>
          <Route path="/citas-admin" exact element={<CitasAdmin></CitasAdmin>}></Route>
          <Route path="/citas-crear" exact element={<CitasCrear></CitasCrear>}></Route>
          <Route path="/citas-editar/:idCita" exact element={<CitasEditar></CitasEditar>}></Route>
          <Route path="/tratamientos-admin" exact element={<TratamientosAdmin></TratamientosAdmin>}></Route>
          <Route path="/tratamientos-crear" exact element={<TratamientosCrear></TratamientosCrear>}></Route>
          <Route path="/tratamientos-editar/:idTratamiento" exact element={<TratamientosEditar></TratamientosEditar>}></Route>
          <Route path="/personas-admin" exact element={<PersonasAdmin></PersonasAdmin>}></Route>
          <Route path="/personas-crear" exact element={<PersonasCrear></PersonasCrear>}></Route>
          <Route path="/personas-editar/:idPersona" exact element={<PersonasEditar></PersonasEditar>}></Route>
          <Route path="/funciones-admin" exact element={<FuncionesAdmin></FuncionesAdmin>}></Route>
          <Route path="/asincronia-admin" exact element={<AsincroniaAdmin></AsincroniaAdmin>}></Route>
        </Routes>
      </Router>
    </Fragment>
  );
}
export default App;