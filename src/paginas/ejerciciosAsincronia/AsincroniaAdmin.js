import React, { /* useState */ } from "react";
import ContentHeader from "../../componentes/ContentHeader";
import Footer from "../../componentes/Footer";
import Navbar from "../../componentes/Navbar";
import SidebarContainer from "../../componentes/SidebarContainer";

const AsincroniaAdmin = () => {


  return (
    <div className="wrapper">
      <Navbar></Navbar>
      <SidebarContainer></SidebarContainer>
      <div className="content-wrapper">
        <ContentHeader
          titulo={"Asincronia"}
          breadCrumb1={"Inicio"}
          breadCrumb2={"Asincronia"}
          breadCrumb3={"Funciones"}
          breadCrumb4={"Citas"}
          breadCrumb5={"Tratamientos"}
          breadCrumb6={"Personas"}
          
          ruta1={"/home"}
          ruta3={"/funciones-admin"}          
          ruta4={"/citas-admin"}
          ruta5={"/tratamientos-admin"}
          ruta6={"/personas-admin"}
          
        />

        <section className="content">
          <div className="card">
            <div className="card-header">
              <div className="card-body">

              </div>
              
            </div>
          </div>                
        </section>        
      </div>    
      <Footer></Footer>
    </div>    
  );
};

export default AsincroniaAdmin;
