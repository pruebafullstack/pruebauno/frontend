import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../componentes/ContentHeader";
import Navbar from "../../componentes/Navbar";
import { useNavigate, useParams } from "react-router-dom";
import APIInvoke from "../../utils/APIInvoke";
import swal from "sweetalert";
import SidebarContainer from "../../componentes/SidebarContainer";
import Footer from "../../componentes/Footer";

const PersonasEditar = () => {
  const navigate = useNavigate();
  const { idPersona  } = useParams();
  console.log("QUE TRAE EL idPersona", idPersona)

  let arreglo = idPersona.split("@");
  console.log("QUE TRAE EL ARREGLO",arreglo)
  const idP = arreglo[0];
  const documentoPersonas =arreglo[1];
  const nombrePersonas = arreglo[2];
  const apellidoPersonas = arreglo[3];

  const [persona, setPersona] = useState({
    documento: documentoPersonas,
    nombre: nombrePersonas,
    apellido: apellidoPersonas,
  });

  const { documento, nombre, apellido} = persona;

  useEffect(() => {
    /* document.getElementById("nombre").focus(); */
  });

  const onChange = (e) => {
    console.log("EEE",e.target.value)
    console.log("aaaa",e.target.apellido)
    console.log("nombre",e.target.nombre)
    setPersona({
      ...persona,
      [e.target.documento]: e.target.value,
      [e.target.name]: e.target.value,
      [e.target.apellido]: e.target.value,
    });
  };

  const editarPersonas = async () => {
    let arreglo = idP.split("@");
    const idPersona = arreglo[0];
    

    const data = {
      documento: persona.documento,
      nombre: persona.nombre,
      apellido: persona.apellido,
    };
    const response = await APIInvoke.invokePUT(
      `/api/personas/${idPersona}`,
      data
    );
    const idPersonaEditado = response.persona._id;

    console.log("response editar", response);

    if (idPersonaEditado !== idPersona) {
      const msg = "No se edito correctamente la Persona";
      swal({
        title: "Error",
        text: msg,
        icon: "error",
        buttons: {
          confirm: {
            text: "Ok",
            value: true,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
        },
      });
    } else {
      navigate("/personas-admin");
      const msg = "La Persona ha sido editada correctamente";
      swal({
        title: "Informacion",
        text: msg,
        icon: "success",
        buttons: {
          confirm: {
            text: "Ok",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true,
          },
        },
      });
    }
  };

  const onSubmit = (e) => {
    e.preventDefault();
    editarPersonas()
  };

  return (
    <div className="wrapper">
      <Navbar></Navbar>
      <SidebarContainer></SidebarContainer>
      <div className="content-wrapper">
        <ContentHeader
          titulo={"Edicion Personas"}
          breadCrumb1={"Inicio"}
          breadCrumb2={"Personas"}
          breadCrumb3={"Citas"}
          breadCrumb4={"Tratamientos"}

          ruta1={"/home"}
          ruta3={"/citas-admin"}
          ruta4={"/tratamientos-admin"}
        />
        {/* </ContentHeader> */}
        <section className="content">
          <div className="card">
            <div className="card-header">
              <div className="card-tools">
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="collapse"
                  title="Collapse"
                >
                  <i className="fas fa-minus" />
                </button>
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="remove"
                  title="Remove"
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            </div>
            {/* 
              
                <div className="card-header">
                  <h3 className="card-title">Bordered Table</h3>
                </div> */}
            {/* /.card-header */}
            <div className="card-body">
              <form onSubmit={onSubmit}>
                <div className="card-body">

                <div className="form-group">
                    <label htmlFor="documento">Documento</label>
                    <input
                      type={"text"}
                      className="form-control"
                      id="documento"
                      name="documento"
                      placeholder="Escriba el documento de la persona"
                      value={documento}
                      onChange={onChange}
                      required
                    ></input>                    
                  </div>

                  <div className="form-group">
                    <label htmlFor="nombre">Nombre</label>
                    <input
                      type={"text"}
                      className="form-control"
                      id="nombre"
                      name="nombre"
                      placeholder="Escriba el nombre de la persona"
                      value={nombre}
                      onChange={onChange}
                      required
                      /* style={"width=225"}  */
                    ></input>                    
                  </div>

                  <div className="form-group">
                    <label htmlFor="apellido">Apellido</label>
                    <input
                      type={"text"}
                      className="form-control"
                      id="apellido"
                      name="apellido"
                      placeholder="Escriba el apellido de la persona"
                      value={apellido}
                      onChange={onChange}
                      required
                    ></input>                    
                  </div>

                </div>
                <div>
                  <button type="submit" className="btn btn-primary">
                    Editar
                  </button>
                  &nbsp;                  
                    <Link class="btn btn-danger" to={"/personas-admin"}>Cancelar</Link>
                </div>
              </form>
            </div>
            {/* /.card-body */}
            <div className="card-footer clearfix">
              <ul className="pagination pagination-sm m-0 float-right">
                <li className="page-item">
                  <Link className="page-link" href="#">
                    «
                  </Link>
                </li>
                <li className="page-item">
                  <Link className="page-link" href="#">
                    1
                  </Link>
                </li>
                <li className="page-item">
                  <Link className="page-link" href="#">
                    2
                  </Link>
                </li>
                <li className="page-item">
                  <Link className="page-link" href="#">
                    3
                  </Link>
                </li>
                <li className="page-item">
                  <Link className="page-link" href="#">
                    »
                  </Link>
                </li>
              </ul>
            </div>

            {/* /.card-body */}
            {/* <div className="card-footer">Footer</div> */}
            {/* /.card-footer*/}
          </div>
        </section>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default PersonasEditar;
