import React, {useEffect, useState} from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../componentes/ContentHeader";
import Navbar from "../../componentes/Navbar";
import { useNavigate } from "react-router-dom";
import APIInvoke from "../../utils/APIInvoke";
import swal from "sweetalert";
import SidebarContainer from "../../componentes/SidebarContainer";
import Footer from "../../componentes/Footer";

const TratamientosCrear = () => {

  const navigate = useNavigate();

  const [tratamiento, setTratamientos] = useState({
    nombre:''
  });

  const { nombre } = tratamiento;

  useEffect(() =>{   
    document.getElementById("nombre").focus()
  })

  const onChange = (e) => {
    
    setTratamientos({
      ...tratamiento,
      [e.target.name]:e.target.value
    })
  }

  const crearTratamiento = async() => {    
    const data = {
      nombre: tratamiento.nombre
    }

    console.log("entro al crear", data)

    const response = await APIInvoke.invokePOST(`/api/tratamientos`, data);
    
    console.log("ResponseTRATAMIENTOS",response)
    
const idTratamiento = response._id;

    if (idTratamiento === ''){
      const msg = "No se creo ningun Tratamiento"
      swal({
        title: 'Error',
        text: msg,
        icon: 'error',
        buttons:{
          confirm:{
          text: 'Ok',
          value: true,
          visible: true,
          className: 'btn btn-danger',
          closeModal: true}
        }
      })
    }else{
      navigate("/tratamientos-admin")
      const msg = "Este Tratamiento fue creada correctamente"
      swal({
        title: 'Informacion',
        text: msg,
        icon: 'success',
        buttons:{
          confirm:{
          text: 'Ok',
          value: true,
          visible: true,
          className: 'btn btn-primary',
          closeModal: true}
        }
      })
      setTratamientos({nombre:''})
    }
  }

  
  const onSubmit = (e) => {
    console.log("entra al boton")
    e.preventDefault();
    console.log("onsubmit", e)
    crearTratamiento();
  }

    return(
        <div className="wrapper">
      <Navbar></Navbar>
      <SidebarContainer></SidebarContainer>
      <div className="content-wrapper">
        <ContentHeader
          titulo={"Creacion Tratamientos"}
          breadCrumb1={"Inicio"}
          breadCrumb2={"Tratamientos"}
          breadCrumb3={"Citas"}
          breadCrumb4={"Personas"}
          ruta1={"/home"}
          ruta3={"/citas-admin"}
          ruta4={"/personas-admin"}
        />
        {/* </ContentHeader> */}
        <section className="content">
          <div className="card">
            <div className="card-header">
             
              <div className="card-tools">
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="collapse"
                  title="Collapse"
                >
                  <i className="fas fa-minus" />
                </button>
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="remove"
                  title="Remove"
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            </div>
            {/* 
              
                <div className="card-header">
                  <h3 className="card-title">Bordered Table</h3>
                </div> */}
                {/* /.card-header */}
                <div className="card-body">
                    <form onSubmit={onSubmit}>
                        <div className="card-body">
                            <div className="form-group">
                                <label htmlFor="nombre">Nombre

                                </label>
                                <input type={"text"} className="form-control" id="nombre"
                                name="nombre" placeholder="Escriba el nombre del Tratamiento"
                                value={nombre} onChange ={onChange} required>
                                </input>
                            </div>
                        </div>
                        <div>
                          <button type="submit" className="btn btn-primary">
                            Crear
                          </button>
                        </div>
                    </form>
                </div>
                {/* /.card-body */}
                <div className="card-footer clearfix">
                  <ul className="pagination pagination-sm m-0 float-right">
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        «
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        1
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        2
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        3
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        »
                      </Link>
                    </li>
                  </ul>
                </div>
              
            

            {/* /.card-body */}
            {/* <div className="card-footer">Footer</div> */}
            {/* /.card-footer*/}
          </div>
        </section>
      </div>
      <Footer></Footer>
    </div>
    )
}

export default TratamientosCrear;