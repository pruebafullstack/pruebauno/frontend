import React, { /* useState */ } from "react";
import ContentHeader from "../../componentes/ContentHeader";
import Footer from "../../componentes/Footer";
import Navbar from "../../componentes/Navbar";
import SidebarContainer from "../../componentes/SidebarContainer";

const FuncionesAdmin = () => {
  //ARREGLOS
  const colores = ["Amarillo","Azul"]
  colores.push("Negro","Verde")
  colores.pop()
  
  colores.forEach(function(element, index){
    console.log(`<li id="${index}">${element}</li>`)
  })

  //OBJETOS
  const persona = {
    nombre:"Jaime",
    apellido:"Gaviria",
    edad: 31,
    pasatiempo:["Ajedrez","Chatear"],
    contacto:{
      email:"jaalgazu",
      movil: 3245657845,
    },
    saludar:function(){
      console.log("una funcion dentro de un objeto es en realidad un metodo", `hola ${this.nombre} ${this.apellido}`)
    }
  }

  console.log("acceder al objeto persona", persona.nombre)
  console.log("acceder al movil del objeto persona", persona.contacto.movil)
  console.log("acceder al arreglo dentro del objeto persona", persona.pasatiempo[1])
  persona.saludar()
  console.log("metodos propios del objeto", Object.keys(persona))
  console.log("metodos propios del objeto", Object.values(persona))
  console.log("metodos propios del objeto", persona.hasOwnProperty("nombre"))

  //EJEMPLO DE CONDICIONALES

  let hora = 10
  let edad = 31

  if(hora<6){
    console.log("Ejemplo de condicionales: deje dormir")
  }else if(hora > 5 && hora < 12){
    console.log("Ejemplo de condicionales: ya no dormir")
  }else {
    console.log("Ejemplo de condicionales: chao papa")
  }

  //OPERADOR TERNARIO (CONDICIONAL)
  let mayor = (edad>=18)
    ?"Adulto" /* ? OPCION VERDADERA */
    :"Nene"   /* : OPCION VERDADERA */

  console.log("EJEMPLO DEL OPERADOR TERNARIO",mayor)

  //SWITCH CASE
  let dia = 2
  switch (dia) {
    case 3:
      console.log("Switch Martes")
      break;
    case 2:
      console.log("Switch Lunes")
      break;
    default:
      console.log("Switch Otro")
      break;
  }

  //CICLOS
  let contador = 10

  console.log("En el While se debe cumplir la condicion para ejecutar el bloque")
  /* while (contador<10){
    contador ++
    console.log("Ciclo While", contador)
  } */
  console.log("En el DoWhile se cumple al menos 1 vez todo el bloque de codigo")
  do {
    console.log("Ciclo DoWhile", contador)
    contador ++
  } while (contador<10);

  for (let index = 0; index < 10; index++) {
    console.log("Ciclo FOR:", index);
    console.log("Ciclo FOR:"+ index)    
  }

  let numeros = [10,20,30,40]
  for (let index = 0; index < numeros.length; index++) {
    //el .length me recorre todas las posiciones del arreglo
    console.count("veces que se ejecuta el ciclo")
    console.log("Ciclo FOR:", numeros[index]);
     
  }

  //FOR IN es exclusivo para los OBJETOS
  for (const propiedad in persona) {    
    console.log("PROPIEDAD DEL OBJETO",propiedad)
    console.log(`Key: ${propiedad}, Value: ${persona[propiedad]}`)
    //permite recorrer o ver que propiedades tiene el objeto
    }
  
    //para VECTORES
  for (const propiedad of persona.pasatiempo) {
    console.log("PROPIEDAD DEL arreglo dentro del OBJETO",propiedad)    
  }
    //PARA CADENAS DE TEXTO
  for (const letra of persona.nombre) {
    console.log("FOR OF DELETREO DEL NOMBRE: "+persona.nombre,letra)    
  }

  //EJERCICIOS DE PRUEBA DE LOS TIPOS DE FUNCIONES
  function funcionDeclarada() {
    console.log("HAS PRESIONADO EL BOTON");
    console.log(
      "probando funciones, esto es una funcion declarada la puedo crear y llamar antes o despues de haberla creado"
    );
    console.log("NO SE RECOMIENDA UTILIZARLAS ACTUALMENTE");
  }
  //funcionDeclarada(); //INVOCACION DE FUNCIONES

  function funcionRetornaValor() {
    return "es necesario almacenar esta funcion en una variable para poderla sacar dentro de un console";
  }
  /* let temporal = funcionRetornaValor; console.log("ESTE ES EL CONSOLE", temporal); */

  function funcionQueRecibeParametros(
    nombre = "NoAsignado",
    apellido = "NoAsignado",
    edad = 0
  ) {
    console.log("A CONTINUACION SE USARA TEMPLATE STRING");
    console.log(`HOLA ${nombre} ${apellido} tienes ${edad} anos`);
  }

  funcionQueRecibeParametros("Jaime", "Gaviria", 31);

  const funcionExpresada = function () {
    console.log(
      "FUNCION EXPRESADA: no se puede invocar en cualquier lugar, pues es una propiedad de las funciones DECLARADAS"
    );
    console.log("ES EL ANTECESOR DE LAS FUNCIONES FLECHA");
  };

  //ARROW FUNCION
  const arrowFuncion1 = () => console.log("ArrowFuncion en una sola linea");
  const arrowFuncion2 = () => {
    console.log("ArrowFuncion en varias lineas, solo incluye llaves al inicio y al final");
  };
  const arrowFuncion3 = (nombre) => {
    console.log(`ArrowFuncion con parametro: ${nombre}`);
  };

  //Prototipos, clases o funcion constructora
  //1 PARTE 
  function Animal(name, address){
    this.nombre = name; 
    this.direccion = address
  }
  //2 PARTE
  //Asignación de un metodo a una clase o Función constructora (prototipo)
  //Ésta asignación se realiza para garantizar un mejor rendimiento
  Animal.prototype.Onomatopeya = function(){
    console.log("sonido que emite el animal")
  }
  const Mascota = new Animal("Katie","Anserma")
  console.log("Esta es mi Mascota", Mascota)

  //3 PARTE
  //HERENCIA
  function Perro(name,address,tamanio){
    this.super = Animal;/* elemento padre es Animal */
    this.super(name,address);
    this.tamanio = tamanio;
  }
  //Perro esta heredando de Animal, en la siguiente linea
  Perro.prototype = new Animal();
  Perro.prototype.constructor = Perro;

  //Sobreescritura de metodos del padre en el hijo
  Perro.prototype.Onomatopeya = function(){
    console.log("Reescribiendo, Gua gua gua gua")
  }

  Perro.prototype.ladra = function(){
    console.log("REguau")
  }

  const Perros = new Perro("Katie2","Anserma2", "Mediano")

  //CLASES
  class Animales{
    constructor(nombre, genero){
      this.name = nombre; 
      this.genere = genero
    }

    Onomatopeyas(){
      console.log("sonido que emite el animal");
    }

    Saludar(){
      console.log(`Hola ${this.name}`);
    }
    
    /* console.log("Esta es mi Mascota", Mascota) */
  }

  //HEREDANDO CON CLASES
  class Raton extends Animales{
    constructor(nombre,genero,tamanNio){
      /* el super llama el constructor del padre */
      super(nombre,genero);
      this.tamanio = tamanNio;
    }
    Saludar(){
      console.log(`Soy un Roedor llamado ${this.name}`)
    }
    Sonido(){
      console.log("No tengo")
    }

  }

  const Minie = new Raton("Minie","Hembra","Pequenio"),
   scooby = new Animales("Scooby","Macho");

  
  
  //MANEJO DE ERRORRES CON TRY/CATCH
  try {
    let numero = "f"   
    if(isNaN(numero)){
      throw new Error ("Asi se crea un error personalizado THROw")
    } 
    console.log(numero*numero)
  } catch (error) {
    console.log("salto al Catch por error sucedido")
    console.log(`Impresion del error encontrado ${error}`)
  }finally{
    console.log("Conveniente utilizar en el Backend, siempre se ejecutara")
    console.log("Por ejemplo para dar cierre a una DB")
  }

  //BREAK/CONTINUE
  console.log("REVISAR SI QUEDO BIEN HECHO")
  let index = [1,2,3,4,5,6,7,8,9]
  for (let i = 0; i < index.length; i++) {
    /* console.log("afuera",index[i]) */
    if(index[i]%2===0){
      /* console.log(index[i]%2) */
      continue
    }
    console.log("Numeros impares: ", index[i])    
  }

  //DESTRUCTURING
  console.log("Destructuring: funciona para arreglos y objetos, pero en los objetos las variables deben ")
  console.log("ser llamadas igual a los atributos o nombres de las propiedades que tiene el objeto en su interior")
  const [uno, dos, tres] = index
  console.log("Ejemplo de destructuracion", uno, dos, tres)

  const{nombre, apellido, pasatiempo} = persona
  console.log("destructuracion sobre objetos", nombre,apellido,pasatiempo)

  //OBJETOS LITERALES
  let name= "katie",
      raza= "koquer";

  const dog = {
    name,
    raza,
    ladra(){
      console.log("gua gua")
      console.log(this)
      /* el this con las arrow function genera inconvenientes, hay que consultar mucho más */
    }
  }

  console.log(dog, dog.ladra)

  //PARAMETROS REST
  //en el siguiente ejemplo el parametro rest permite hacer algo infinito como acontinuación
  //consultar mas pues no lo entendi muy bien

  function sumar(a,b,...c){
    let total=a+b;

    c.forEach(function(n){
      total+=n
    });
    return total
  }

  console.log("Parametros REST",sumar(4,5,6,7,8));

  //OPERADOR SPREAD (Operador de propagacion)
  const arr1 = [1,2,3,4,5],
        arr2 = [6,7,8,9,0],
        arr3 = [...arr1,...arr2]
  console.log("Spread ", arr3)
  
  return (
    <div className="wrapper">
      <Navbar></Navbar>
      <SidebarContainer></SidebarContainer>
      <div className="content-wrapper">
        <ContentHeader
          titulo={"Listado de Funciones"}
          breadCrumb1={"Inicio"}
          breadCrumb2={"Funciones"}
          breadCrumb3={"Citas"}
          breadCrumb4={"Tratamientos"}
          breadCrumb5={"Personas"}
          ruta1={"/home"}
          ruta3={"/citas-admin"}
          ruta4={"/tratamientos-admin"}
          ruta5={"/funciones-admin"}
        />

        <section className="content">
          <div className="card">
            <div className="card-header">
              <div className="card-body">
                <p>
                  Los botones azules  son funciones declaradas: se pueden invocar
                  en cualquier lugar
                </p>
                <p>
                  Los botones rojos son funciones expresadas: se deben invocar
                  despues de haber sido declaradas
                </p>
                <button
                  className="btn btn-primary btn-sm"
                  onClick={funcionDeclarada}
                >
                  Funcion declarada
                </button>
                &nbsp;
                <button
                  className="btn btn-primary btn-sm"
                  onClick={() => {
                    let temporal = funcionRetornaValor;
                    console.log("ESTE ES EL CONSOLE", temporal);
                  }}
                >
                  Funcion con return
                </button>
                &nbsp;
                <button
                  className="btn btn-primary btn-sm"
                  onClick={funcionQueRecibeParametros}
                >
                  Funcion con Parametros
                </button>
                &nbsp;
                <button
                  className="btn btn-danger btn-sm"
                  onClick={funcionExpresada}
                >
                  Funcion Expresada
                </button>
                &nbsp;
                <button
                  className="btn btn-danger btn-sm"
                  onClick={arrowFuncion1}
                >
                  Funcion Anonima Expresada (F.A.E.) ARROW1
                </button>
                &nbsp;
                <button
                  className="btn btn-danger btn-sm"
                  onClick={arrowFuncion2}
                >
                  F. A. E. ARROW2
                </button>
                &nbsp;
                <button
                  className="btn btn-danger btn-sm"
                  onClick={arrowFuncion3("Jaime")}
                >
                  F. A. E. ARROW con Parametros
                </button>
                &nbsp;
                <button
                  className="btn btn-danger btn-sm"
                  onClick={() => {
                    console.log(`Intentar dar algun parametro desde aca:`)
                    console.log(`Revisiarlo de nuevo`)
                  }}
                >
                  F. A. E. ARROW con Parametros
                </button>
                &nbsp;
                <button
                  className="btn btn-danger btn-sm"
                  onClick={() => {
                    console.log("Esta es mi Mascota", Mascota)
                    Mascota.Onomatopeya()/*Llamado del metodo de la clase animal */
                  }}
                >
                  PROTOTIPO
                </button>
                &nbsp;
                <button
                  className="btn btn-danger btn-sm"
                  onClick={() => {
                    console.log("Esta es mi Mascota", Perros)
                    Perros.Onomatopeya()
                    Perros.ladra()
                  }}
                >
                  HERENCIA
                </button>
                &nbsp;
                <button
                  className="btn btn-danger btn-sm"
                  onClick={() => {
                    console.log("Esta es mi Mascota", scooby , Minie)
                     Minie.Saludar()
                     Minie.Sonido()
                    /*Perros.ladra() */
                  }}
                >
                  CLASE
                </button>
                &nbsp;
                <button
                  className="btn btn-danger btn-sm"
                  onClick={() => {
                    console.log(console)
                    console.dir(document)                    
                  }}
                >
                  console
                </button>
              </div>
              
            </div>
          </div>                
        </section>        
      </div>    
      <Footer></Footer>
    </div>    
  );
};

export default FuncionesAdmin;
